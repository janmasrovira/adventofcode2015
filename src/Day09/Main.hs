module Day09.Main where

import           Control.Monad
import           Data.List
import           Data.Map.Strict (Map)
import qualified Data.Map.Strict as Map
import           Data.Maybe
import qualified Data.Set        as Set
import           Data.Tuple.HT

type Distance = Integer
type City = String
type Road = (City, City, Distance)
type RoadMap = ([City], Map (City, City) Distance)

ordNub :: Ord a => [a] -> [a]
ordNub = Set.toList . Set.fromList

inputFile :: String
inputFile = "src/Day09/input.txt"

readRoad :: String -> Road
readRoad r = let [a,_,b,_,d] = words r
             in (a, b, read d)

mkRoadMap :: [Road] -> RoadMap
mkRoadMap xs = (ordNub (map fst3 xs ++ map snd3 xs)
               , Map.fromList $ concat [ [((a,b), d), ((b, a), d)]| (a,b,d) <- xs])

tsp :: ([Distance] -> a) -> RoadMap -> a
tsp agg (cs, dists) = agg (mapMaybe (totalDist dists) (permutations cs))

totalDist :: Map (City, City) Distance -> [City] -> Maybe Distance
totalDist m trace = sum <$> zipWithM (curry getDist) trace (tail trace)
  where getDist = (`Map.lookup`m)

part1 :: IO Distance
part1 = tsp minimum . mkRoadMap . map readRoad . lines <$> readFile inputFile

part2 :: IO Distance
part2 = tsp maximum . mkRoadMap . map readRoad . lines <$> readFile inputFile
