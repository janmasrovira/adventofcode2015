module Day06.Main where

import           Data.List             (foldl')
import           Data.Map.Strict       (Map)
import qualified Data.Map.Strict       as Map
import           Data.Set              (Set)
import qualified Data.Set              as Set
import           Day06.Parser
import           Text.Megaparsec


type Grid = Set Pos
type Grid' = Map Pos Int


inputFile :: FilePath
inputFile = "src/Day6/input.txt"

part1 :: IO Int
part1 = do
  r <- readProgram
  case r of
    Left err -> error (show err)
    Right prog -> return $ Set.size (runProgram prog)


readProgram :: IO (Either ParseError Program)
readProgram = parseFromFile program inputFile

inside :: Rectangle -> [Pos]
inside ((x, y), (x', y')) = [ (a, b) | a <- [x..x'], b <- [y..y']]

toggle :: Pos -> Grid -> Grid
toggle p g
  | p `Set.member` g = turnOff p g
  | otherwise = turnOn p g

turnOn :: Pos -> Grid -> Grid
turnOn = Set.insert

turnOff :: Pos -> Grid -> Grid
turnOff = Set.delete

runProgram :: Program -> Grid
runProgram = foldl' runInstruction Set.empty

runInstruction :: Grid -> Instruction -> Grid
runInstruction g (Toggle r) = foldr toggle g (inside r)
runInstruction g (Turn on r) =  foldr (if on then turnOn else turnOff) g (inside r)

toggle' :: Pos -> Grid' -> Grid'
toggle' = turnOn' 2

turnOn' :: Int -> Pos -> Grid' -> Grid'
turnOn' k = Map.alter (Just . alt)
  where
    alt Nothing = k
    alt (Just x) = x + k

turnOff' :: Pos -> Grid' -> Grid'
turnOff' = Map.update update
  where
    update 1 = Nothing
    update x = Just (x - 1)

runProgram' :: Program -> Grid'
runProgram' = foldl' runInstruction' Map.empty

runInstruction' :: Grid' -> Instruction -> Grid'
runInstruction' g (Toggle r) = foldr toggle' g (inside r)
runInstruction' g (Turn on r) =  foldr (if on then turnOn' 1 else turnOff') g (inside r)

part2 :: IO Int
part2 = do
  r <- readProgram
  case r of
    Left err -> error (show err)
    Right prog -> return $ sum $ Map.elems (runProgram' prog)
