module Day06.Parser where

import Control.Monad
import Data.ByteString.Char8 (ByteString)
import Text.Megaparsec
import Text.Megaparsec.Lexer (decimal)


type Pos = (Int, Int)

type Rectangle = (Pos, Pos)

data Instruction =
  Toggle Rectangle
  | Turn Bool Rectangle deriving (Show)

type Program = [Instruction]

type ParsecB = Parsec ByteString

instruction :: ParsecB Instruction
instruction = try itoggle
              <|> iturn

itoggle :: ParsecB Instruction
itoggle = do
  void (string "toggle")
  space
  r <- rectangle
  return (Toggle r)

iturn :: ParsecB Instruction
iturn = do
 void (string "turn")
 space
 b <- onOff
 r <- rectangle
 return (Turn b r)

onOff :: ParsecB Bool
onOff = do
  b <- (try (string "on") >> return True)
       <|> (string "off" >> return False)
  space
  return b

rectangle :: ParsecB Rectangle
rectangle = do
  p1 <- pos
  void (string "through")
  space
  p2 <- pos
  space
  return (p1, p2)

pos :: ParsecB Pos
pos = do
  x <- int
  void (char ',')
  y <- int
  space
  return (x, y)


int :: ParsecB Int
int = fmap fromIntegral decimal

program :: ParsecB Program
program = many instruction
