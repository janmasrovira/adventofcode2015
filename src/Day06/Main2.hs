-- | Uses arrays instead of maps and sets (much more efficient).
-- compile it! otherwise is too slow
module Day06.Main2 where

import           Control.Monad
import           Control.Monad.Primitive
import           Data.ByteString.Char8   (ByteString)
import           Data.Ix
import           Data.List               (foldl')
import           Data.Vector.Mutable     (MVector)
import qualified Data.Vector.Mutable     as V
import           Day06.Parser
import           Text.Megaparsec

type Grid s a = MVector (PrimState s) (MVector (PrimState s) a)

-- from 0 to 999
gridSide :: Int
gridSide = 1000

inputFile :: FilePath
inputFile = "src/Day6/input.txt"

part1 :: IO Int
part1 = do
  r <- readProgram
  case r of
    Left err -> error (show err)
    Right prog -> runProgram False runInstruction prog >>= (fmap (length . filter id) . elems2D)

emptyGrid :: PrimMonad m => a -> m (Grid m a)
emptyGrid z = V.replicateM gridSide (V.replicate gridSide z)

runProgram :: PrimMonad m => a -> (Grid m a -> Instruction -> m ()) ->
              Program -> m (Grid m a)
runProgram z runInstr ins = do
  g <- emptyGrid z
  forM_ ins (runInstr g)
  return g

readProgram :: IO (Either ParseError Program)
readProgram = parseFromFile program inputFile

inside :: Rectangle -> [Pos]
inside ((x, y), (x', y')) = [ (a, b) | a <- [x..x'], b <- [y..y']]

read2D :: PrimMonad m => Grid m a -> Pos -> m a
read2D g (i, j) = V.read g i >>= flip V.read j

write2D :: PrimMonad m => Grid m a -> Pos -> a -> m ()
write2D g (i, j) x = V.read g i >>= \h -> V.write h j x

update2D :: PrimMonad m => (a -> a) -> Grid m a -> Pos -> m ()
update2D f g (i, j) = do
  h <- V.read g i
  x <- V.read h j
  V.write h j (f x)

-- assumes grid is a square (n x n)
elems2D :: PrimMonad m => Grid m a -> m [a]
elems2D g = sequence [ read2D g ij | ij <- range ((0, 0), (n - 1, n - 1))]
  where n = V.length g

toggle :: PrimMonad m => Grid m Bool -> Pos -> m ()
toggle = update2D not

turn :: PrimMonad m => Grid m Bool -> Pos -> Bool -> m ()
turn = write2D


runInstruction :: PrimMonad m => Grid m Bool -> Instruction -> m ()
runInstruction g (Toggle r) = forM_ (inside r) (toggle g)
runInstruction g (Turn on r) =  forM_ (inside r) $ \p -> turn g p on


part2 :: IO Int
part2 = do
  r <- readProgram
  case r of
    Left err -> error (show err)
    Right prog -> runProgram 0 runInstruction' prog >>= (fmap sum . elems2D)

runInstruction' :: PrimMonad m => Grid m Int -> Instruction -> m ()
runInstruction' g (Toggle r) = forM_ (inside r) (toggle' g)
runInstruction' g (Turn on r) =  forM_ (inside r) $ \p -> turn' g p on

toggle' :: PrimMonad m => Grid m Int -> Pos -> m ()
toggle' = update2D (+2)

turn' :: PrimMonad m => Grid m Int -> Pos -> Bool -> m ()
turn' g p on = update2D (if on then succ else  max 0 . pred) g p
