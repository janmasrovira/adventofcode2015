module Day20.Main where

import Data.List

input :: Int
input = 33100000

intsqrt :: Int -> Int
intsqrt = floor . sqrt . fromIntegral

part1 :: Int
part1 = (+1) $ length $ takeWhile (< input) $ map house [1..]

-- presents in house n
house :: Int -> Int
house = (*10) . sum . divisors

-- divisors not in order
-- n > 0
divisors :: Int -> [Int]
divisors n = nub [1, n] ++ concat [ nub [d, q]
                                  | d <- [2 .. intsqrt n]
                                  , let (q, r) = quotRem n d , r == 0]
