module Day10.Main where

import Data.List
import Control.Arrow

input :: String
input = "1321131112"

say :: String -> String
say = concatMap (uncurry (++) . (show . length &&& take 1)) . group

part1 :: Int
part1 = length (iterate say input !! 40)

part2 :: Int
part2 = length (iterate say input !! 50)
