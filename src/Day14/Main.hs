module Day14.Main where

import           Data.List
import qualified Data.Map.Strict as Map

type Speed = Int
type Rest = Int
type Stamina = Int
type Time = Int
type Stats = (Speed, Stamina, Rest)

inputFile :: String
inputFile = "src/Day14/input.txt"

readReindeer :: String -> Stats
readReindeer x = (read speed, read stamina, read rest)
  where
    [name, can, fly, speed, kms, for, stamina, seconds
      , but, then_, must, rest_, for2, rest, seconds2] = words x

time :: Time
time = 2503

part1 :: IO Int
part1 = maximum . map (run time . readReindeer) . lines <$> readFile inputFile

run :: Time -> Stats -> Int
run t (v, s, r) = fullCycles*cycleDist + min remCycle s * v
  where
    cycleTime = s + r
    cycleDist = v*s
    (fullCycles, remCycle) = t`quotRem`cycleTime

whichMax :: Ord a => [a] -> [Int]
whichMax xs = map fst $ filter ((==m) . snd) $ zip [0..] xs
  where m = maximum xs

part2 :: IO Int
part2 = maximum . map snd . count . concat . (map leaders [1..time] <*>)
        . pure . map readReindeer . lines
        <$> readFile inputFile

leaders :: Int -> [Stats] -> [Int]
leaders i = whichMax . map (run i)

count :: Ord a => [a] -> [(a, Int)]
count = Map.toList . foldl' (\a k -> Map.insertWith (+) k 1 a) mempty
