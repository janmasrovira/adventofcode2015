module Day05.Main where

import Data.List (transpose)


inputFile :: String
inputFile = "src/Day05/input.txt"

vowels :: [Char]
vowels = "aeiou"

isVowel :: Char -> Bool
isVowel = (`elem` vowels)

abc :: [Char]
abc = ['a'..'z']

cond1 :: String -> Bool
cond1 = (>=3) . length . filter isVowel

cond2 :: String -> Bool
cond2 x = any (\c -> [c,c] `subseqOf`x) abc

cond3 :: String -> Bool
cond3 x = all (\s -> not (s `subseqOf` x)) ["ab", "cd", "pq", "xy"]

subseqOf :: Eq a => [a] -> [a] -> Bool
subseqOf x y = x `elem` seqsOf (length x) y

seqsOf :: Int -> [a] -> [[a]]
seqsOf k l = takeWhile ((>=k) . length) $ transpose $ map (`drop` l) [0..k - 1]

isNice :: String -> Bool
isNice x = cond1 x && cond2 x && cond3 x

part1 :: IO Int
part1 = length . filter isNice . lines <$> readFile inputFile

part2 :: IO Int
part2 = length . filter isNice2 . lines <$> readFile inputFile

isNice2 :: String -> Bool
isNice2 x = cond1' x && cond2' x

seqsOf' :: Int -> [a] -> [( [a], [a] )]
seqsOf' k l = takeWhile ((>=k) . length . fst) $ map (splitAt k . (`drop` l)) [0..]

cond1' :: String -> Bool
cond1' = any (uncurry subseqOf) . seqsOf' 2

cond2' :: String -> Bool
cond2' = any isPalindrome . seqsOf 3

isPalindrome :: Eq a => [a] -> Bool
isPalindrome x = x == reverse x
