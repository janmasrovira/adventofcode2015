module Day12.Main where

import           Data.Aeson
import qualified Data.ByteString.Lazy.Char8 as B
import qualified Data.HashMap.Lazy          as Map
import           Data.Scientific
import qualified Data.Vector                as V

inputFile :: String
inputFile = "src/Day12/input.txt"

getValue :: IO (Maybe Value)
getValue = decode <$> B.readFile inputFile

part1 :: IO Int
part1 = maybe (error "parse error") (floor . sumNums (const True)) <$> getValue

part2 :: IO Int
part2 = maybe (error "parse error") (floor . sumNums (not . isRed)) <$> getValue

sumNums :: (Value -> Bool) -> Value -> Scientific
sumNums _ (Number n) = n
sumNums f o@(Object m)
  | f o = sum (map (sumNums f) (Map.elems m))
  | otherwise = 0
sumNums f (Array v) = V.foldl' (\a x -> a + sumNums f x) 0 v
sumNums _ _ = 0

isRed :: Value -> Bool
isRed (Object m) = "red" `elem` Map.elems m
isRed _ = False
