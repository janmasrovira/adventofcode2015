module Day04.Main where

import           Crypto.Hash.MD5
import           Data.ByteString.Base16
import           Data.ByteString.Char8  (ByteString, pack, append)
import qualified Data.ByteString.Char8  as C


md5hex :: ByteString -> ByteString
md5hex = encode . hash

input :: ByteString
input = "ckczppom"

int2BS :: Int -> ByteString
int2BS = pack . show

startsNzeros :: Int -> ByteString -> Bool
startsNzeros n x = C.take n x == (C.replicate n '0')

part1 = head [ n | n <- [0..],
               startsNzeros 5 (md5hex $ input `append` int2BS n)]

part2 = head [ n | n <- [0..],
               startsNzeros 6 (md5hex $ input `append` int2BS n)]
