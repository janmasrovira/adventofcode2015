module Day03.Main where

import Control.Arrow
import Data.Set      (Set, fromList, size, union)

type Dir = Char

type Pos = (Int, Int)

inputFile :: FilePath
inputFile = "src/Day03/input.txt"

readDirs :: IO [Dir]
readDirs = readFile inputFile

move :: Dir -> Pos -> Pos
move '<' = first pred
move '>' = first succ
move '^' = second succ
move 'v' = second pred
move _ = id

visited :: [Dir] -> Set Pos
visited = fromList . scanl (flip move) (0, 0)

part1 :: IO Int
part1 = size . visited <$> readDirs

splitDirs :: [Dir] -> ([Dir], [Dir])
splitDirs [] = ([], [])
splitDirs (x:xs) = (x:a, b)
  where (b, a) = splitDirs xs

part2 :: IO Int
part2 = do
  (a, b) <- splitDirs <$> readDirs
  return $ size (visited a `union` visited b)
