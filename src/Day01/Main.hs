module Day01.Main where

import qualified Data.ByteString.Lazy.Char8 as C

inputFile :: FilePath
inputFile = "src/Day01/input.txt"

part1 :: IO Int
part1 = C.foldl' (flip f) 0 <$> C.readFile inputFile

part2 :: IO Int
part2 = g . C.unpack <$> C.readFile inputFile

g :: String -> Int
g = length . takeWhile (>=0) . scanl (flip f) 0


f :: Char -> Int -> Int
f ')' = pred
f '(' = succ
f _ = id
