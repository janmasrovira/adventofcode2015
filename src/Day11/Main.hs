module Day11.Main where

import           Data.Bimap        ((!), (!>), Bimap)
import qualified Data.Bimap        as Bmap
import           Data.List
import           Data.List.Ordered
import           Data.String
import           Numeric


newtype Password = Password Integer
                   deriving (Enum)

instance Show Password where
  show (Password n) = showIntAtBase base (digitMap !) n ""

instance IsString Password where
  fromString s = Password $ sum [ toInteger (digitMap !> d)*base^e
                                | (e, d) <- zip [0::Int ..] (reverse s)]


part1 :: Password
part1 = (head . filter conds) [input..]

part2 :: Password
part2 = ((!!1) . filter conds) [input..]

conds :: Password -> Bool
conds x = cond1 x && cond3 x

input :: Password
input = "vzbxkghb"

abc :: [Char]
abc = ['a'..'z'] `minus'` "ilo"

cond1 :: Password -> Bool
cond1 = any isConsecutive . sublistsk 3 . show

cond3 :: Password -> Bool
cond3 p = f (show p) 2
  where
    f _ 0 = True
    f (a:b:xs) n
      | a == b = f xs (n - 1)
      | otherwise = f (b:xs) n
    f _ _ = False

base :: Integer
base = genericLength abc

digitMap :: Bimap Int Char
digitMap = Bmap.fromList (zip [0..] abc)

sublistsk :: Int -> [a] -> [[a]]
sublistsk k = takeWhile ((==k) . length) . map (take k) . tails

isConsecutive :: (Enum a, Eq a) => [a] -> Bool
isConsecutive [] = True
isConsecutive (x:xs) = xs `isPrefixOf` [succ x ..]
