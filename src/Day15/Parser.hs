{-# LANGUAGE TemplateHaskell #-}
module Day15.Parser where

import Control.Lens
import Control.Monad
import Text.Megaparsec
import Text.Megaparsec.ByteString.Lazy
import Text.Megaparsec.Lexer           (signed, decimal)

data Stats = Stats {
  _capacity :: Int
  , _durability :: Int
  , _flavour :: Int
  , _texture :: Int
  , _calories :: Int
  } deriving (Show)
makeLenses ''Stats

word :: Parser String
word = some letterChar <* space

colon :: Parser ()
colon = void (char ':') <* space

comma :: Parser ()
comma = void (char ',') <* space

int :: Parser Int
int = fromInteger <$> signed space decimal

ingredient :: Parser Stats
ingredient = do
  word >> colon
  [cap, dur, fla, tex, cal] <- count 5 $ word >> int <* ((comma >> space) <|> void newline)
  return Stats { _capacity = cap
               , _durability = dur
               , _flavour = fla
               , _texture = tex
               , _calories = cal}

ingredients :: Parser [Stats]
ingredients = some ingredient
