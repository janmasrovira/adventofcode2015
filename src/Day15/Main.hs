module Day15.Main where

import Control.Lens
import Day15.Parser
import Text.Megaparsec
import Control.Arrow

type Cookie = [(Int, Stats)]

inputFile :: FilePath
inputFile = "src/Day15/input.txt"

part1 :: IO (Either ParseError Int)
part1 = right (bestScore score) <$> parseFromFile ingredients inputFile

part2 :: IO (Either ParseError Int)
part2 = right (bestScore score2) <$> parseFromFile ingredients inputFile

bestScore :: (Cookie -> Int) -> [Stats] -> Int
bestScore score s = let q = sums (length s) spoons
                    in maximum $ map (score . (`zip` s)) q

spoons :: Int
spoons = 100

sums :: Int -> Int -> [[Int]]
sums 0 0 = [[]]
sums 1 n = [[n]]
sums l n = concatMap f [0..n]
  where
    f :: Int -> [[Int]]
    f h = map (h:) (sums (l - 1) (n - h))


score2 :: Cookie -> Int
score2 c
  | property (view calories) == 500 =
      property (view capacity)
      * property (view durability)
      * property (view flavour)
      * property (view texture)
  | otherwise = 0
  where
    property get = max 0 (sum [ k * get s | (k, s) <- c])

score :: Cookie -> Int
score c = property (view capacity)
          * property (view durability)
          * property (view flavour)
          * property (view texture)
  where
    property get = max 0 (sum [ k * get s | (k, s) <- c])
