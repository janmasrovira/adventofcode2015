module Day08.Main where

import Control.Arrow

inputFile :: String
inputFile = "src/Day08/input.txt"

part1 :: IO Int
part1 = sum . map difference . lines <$> readFile inputFile

difference :: String -> Int
difference = uncurry (-) . (length &&& escape)

escape :: String -> Int
escape "" = 0
escape ('\\':'x':_:_:s) = 1 + escape s
escape ('\\':_:s) = 1 + escape s
escape ('\"':s) = escape s
escape (_:s) = 1 + escape s

unescape :: String -> Int
unescape = length . show

part2 :: IO Int
part2 = sum . map difference2 . lines <$> readFile inputFile

difference2 :: String -> Int
difference2 = uncurry (-) . (unescape &&& length)
