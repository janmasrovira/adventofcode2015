module Day07.Parser where

import Control.Monad
import Data.ByteString.Char8 as B (ByteString)
import Text.Megaparsec
import Text.Megaparsec.Lexer (decimal)
import Data.Word

type ParsecB = Parsec ByteString
type Wire = String
type Signal = Word32
data Input = Signal Signal
           | Wire Wire
           deriving (Show)

data Gate = And Input Input
          | Or Input Input
          | LShift Input Int
          | RShift Input Int
          | Not Input
          | Id Input
          deriving (Show)

data Connection = Connection Gate Wire
                deriving (Show)

type Circuit = [Connection]

circuit :: ParsecB Circuit
circuit = many connection

int :: ParsecB Word32
int = fmap fromIntegral decimal <* space

kword :: String -> ParsecB ()
kword s = void (try (string s) <* space)

wire :: ParsecB Wire
wire = some letterChar <* space

gate :: ParsecB Gate
gate = notGate <|> biGate

notGate :: ParsecB Gate
notGate = kword "NOT" >> fmap Not input

biGate :: ParsecB Gate
biGate = do
  a <- input
  choice [ kword "AND" >> fmap (And a) input
         , kword "OR" >> fmap (Or a) input
         , kword "LSHIFT" >> fmap (LShift a . fromIntegral) int
         , kword "RSHIFT" >> fmap (RShift a . fromIntegral) int
         , return (Id a)]

input :: ParsecB Input
input = (Signal <$> int)
        <|> (Wire <$> wire)

connection :: ParsecB Connection
connection = gate >>= (\v -> arrow >> fmap (Connection v) wire)

arrow :: ParsecB ()
arrow = kword "->"
