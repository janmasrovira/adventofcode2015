module Day07.Main where

import           Control.Monad.Reader
import           Control.Monad.State.Lazy
import           Data.Bits
import           Data.Either.Extra
import           Data.Map.Strict          (Map, (!))
import qualified Data.Map.Strict          as Map
import           Day07.Parser
import           Text.Megaparsec          (parseFromFile, ParseError)

type Signals = Map Wire Signal
type Gates = Map Wire Connection
type MCircuit = ReaderT Gates (State Signals)

inputFile :: FilePath
inputFile = "src/Day07/input.txt"

readCircuit :: IO (Either ParseError Circuit)
readCircuit = parseFromFile circuit inputFile

part1 :: IO Signal
part1 = do
  pr <- readCircuit
  case pr of
    Left err -> error (show err)
    Right c -> return $ evalState (runReaderT (getSignal "a") (assemble c)) Map.empty

out :: Connection -> Wire
out (Connection _ w) = w

part2 :: IO Signal
part2 = do
  sa <- part1
  c <- (Connection (Id (Signal sa)) "b":) . filter ((/= "b") . out) . fromRight <$> readCircuit
  return $ evalState (runReaderT (getSignal "a") (assemble c)) Map.empty

assemble :: Circuit -> Gates
assemble cc = Map.fromList [ (w, c) | c@(Connection _ w) <- cc ]

getConnection :: Wire -> MCircuit Connection
getConnection w = asks (!w)

getSignal :: Wire -> MCircuit Signal
getSignal w = do
  ss <- get
  let m = Map.lookup w ss
  case m of
    Just x -> return x
    _ -> do conn <- getConnection w
            y <- eval conn
            setSignal w y

setSignal :: Wire -> Signal -> MCircuit Signal
setSignal w s = get >>= put . Map.insert w s >> return s

getInput :: Input -> MCircuit Signal
getInput (Signal s) = return s
getInput (Wire w) = getSignal w

eval :: Connection -> MCircuit Signal
eval (Connection (And a b) w) = bi w a b (.&.)
eval (Connection (Or a b) w) = bi w a b (.|.)
eval (Connection (LShift a l) w) = uni w a (`shiftL` l)
eval (Connection (RShift a l) w) = uni w a (`shiftR` l)
eval (Connection (Not a) w) = uni w a complement
eval (Connection (Id i) w) = uni w i id

uni :: Wire -> Input -> (Signal -> Signal) -> MCircuit Signal
uni w a op = do
  va <- getInput a
  setSignal w (op va)

bi :: Wire -> Input -> Input -> (Signal -> Signal -> Signal) -> MCircuit Signal
bi w a b op = do
  va <- getInput a
  vb <- getInput b
  setSignal w (va `op` vb)
