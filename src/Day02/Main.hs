module Day02.Main where

import Data.List.Split
import Data.List

type Dims = (Int, Int, Int)

inputFile :: FilePath
inputFile = "src/Day02/input.txt"

readDims :: IO [Dims]
readDims = map parseDims . lines
           <$> readFile inputFile

part1 :: IO Int
part1 = sum . map area
        <$> readDims

part2 :: IO Int
part2 = sum . map ribbon
        <$> readDims

parseDims :: String -> Dims
parseDims = (\[a,b,c] -> (a, b, c)) . map read . wordsBy (=='x')

area :: Dims -> Int
area (l, w, h) = let s = [l*w, w*h, h*l]
                 in 2 * sum s + minimum s

ribbon :: Dims -> Int
ribbon (l, w, h) = let s = [l, w, h]
                   in 2 * sum (take 2 (sort s)) + product s
