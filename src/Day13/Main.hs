module Day13.Main where

import           Data.List
import           Data.Map.Strict (Map)
import qualified Data.Map.Strict as Map
import           Data.Maybe
import qualified Data.Set        as Set

type Name = String
type OrdPair a = (a, a)
type Relation = (OrdPair Name, Int)
type Relations = Map (OrdPair Name) Int

inputFile :: String
inputFile = "src/Day13/input.txt"

part1 :: IO Int
part1 = findBest . relations . map readRelation . lines <$> readFile inputFile

part2 :: IO Int
part2 = findBest . relations . map readRelation . addMe . lines <$> readFile inputFile
  where
    addMe = ("X would gain 0 happiness units by sitting next to X.":)


ordPair :: Ord a => a -> a -> OrdPair a
ordPair a b
  | a <= b = (a, b)
  | otherwise = (b, a)

nubOrd :: [Name] -> [Name]
nubOrd = Set.toList . Set.fromList

newRelation :: Name -> Name -> Int -> Relation
newRelation a b k = (ordPair a b, k)

readRelation :: String -> Relation
readRelation l = newRelation a b k
  where
    [a, would, gainlose, n, happiness, units, by, sitting, next, to, b] = words (init l)
    k
      | gainlose == "gain" = read n
      | otherwise = - read n

findBest :: Relations -> Int
findBest rel = maximum . map (happiness rel) . permutations $ names
  where
    names = nubOrd $ concatMap (\(a, b) -> [a,b]) $ Map.keys rel

happiness :: Relations -> [Name] -> Int
happiness rel names = sum $ zipWith (interaction rel) names' (tail names')
  where
    names' = names ++ [head names]

interaction :: Relations -> Name -> Name -> Int
interaction rel a b = fromMaybe 0 (Map.lookup (ordPair a b) rel)

relations :: [(OrdPair Name, Int)] -> Relations
relations = foldl' (\m (k, n) -> Map.insertWith (+) k n m) Map.empty
